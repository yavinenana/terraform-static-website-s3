output "aws_route53_record-name" {
  value = module.website.aws_route53_record_name
}
output "aws_route53_record-fqdn" {
  value = module.website.aws_route53_record_name
}
output "aws_route53_zone-name_server" {
  value = module.website.aws_route53_zone_name_servers
}
output "aws_s3_bucket-bucket_domain_name" {
  value = module.website.aws_s3_bucket_bucket_domain_name
}

output "nameservers" {
  value = module.website.nameservers
}
