# it's like main.tf
# ./terraform.tf
variable "aws_region" {
  type = string
}
variable "miprofile" {}
variable "domain_name" {
  type = string
}
variable "env" {}
provider "aws" {
  region = var.aws_region
  profile = var.miprofile
  version = "~> 2.52"
}
module "website" {
#  source = "./.deploy/terraform/static-site"
  source = "./tfmodules/static-site/"
  domain_name = var.domain_name
  env = var.env
}
