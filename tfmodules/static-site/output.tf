output "aws_route53_record_name" {
  value = aws_route53_record.www.name
}
output "aws_route53_record_fqdn" {
  value = aws_route53_record.www.fqdn
}
output "aws_route53_zone_name_servers" {
  value = aws_route53_zone.primary.name_servers
}
output "aws_s3_bucket_bucket_domain_name" {
  value = aws_s3_bucket.website_bucket.*.bucket_domain_name
}
output "nameservers" {
  value = aws_route53_zone.primary.name_servers
}
