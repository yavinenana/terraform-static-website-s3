# website_domain - The domain of the website endpoint, if the bucket is configured with a website. If not, this will be an empty string. This is used to create Route 53 alias records.
# .deploy/terraform/static-site/s3.tf
resource "aws_s3_bucket" "website_bucket" {
  bucket = var.domain_name
  acl = "public-read"
  policy = data.aws_iam_policy_document.website_policy.json
  lifecycle {
    prevent_destroy = false
  }
  force_destroy = true
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  tags = {
    environment = var.env
  }
}
