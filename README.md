REPOSITORIO PÚBLICO - TERRAFORM EKS
# Version terraform 
  vs terragrunt (but it repo dont support terragrunt apply)
	terraform-v0.13.5     VS    terragrunt-v0.25.0
	version terraform to download: terraform_0.13.5_linux_amd64.zip
# Terraform v0.13.5

# vpc-produseast1
 - run commands: 
# to download plugin aWS provider to terraform
 . terraform init
# planing stack
 . terraform plan
# deploy stack
 . terraform apply 
# destroy stack - BE CAREFUL TO PRODUCTION
 . terraform destroy 
 . terraform destroy -force    ... deprecated in v11
 . terraform destroy -auto-approve
#  to get state and output 
. terraform state list
# to update new modules
. terraform get -update=true

# ENVIROMENTS STATES
# PUT BEFORE TF_LOG=DEBUG to enable mode debug
. terraform workspace new    $WORKSPACE
. terraform workspace list
. terraform workspace select $WORKSPACE
. terraform plan    -var-file terraform.tfvars
. terraform apply   -var-file terraform.tfvars
. terraform destroy -var-file terraform.tfvars

# ###########################
- Run main.tf that it has modules to import code from tfmodules/ec2/ 
- This it will call resources of ec2 from tfmodules/ec2
- it will create 2 ec2 for resource and 1 sec group

terraform.tfvars --> it's like constructor from code (almacena parametros , pero igual puedes cambiarlas al instanciarlas
variables.tf or terraform.tf  --> define variables to use in declarator in resource like to main.tf

[FEAT] ROUTE53-ZONE | S3-BUCKET | IAM POLICY -- to server static website in s3 bucket 

# ROUTE 53
. resource aws_route53_zone , resource aws_route53_record
# S3 BUCKET
. resource s3_bucket, destroy force to storage index.html
# IAM POLICY
. permiss * AWS to GET objects like to index..
